from typing import Set
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS

import mosaik_api


META = {
    "type": "event-based",
    "models": {
        # DateTime is a singleton model. Use it to set the timestamp for the current
        # step’s data. If no timestamp is provided, the current time is used instead.
        "DateTime": {
            "public": True,
            "any_inputs": False,
            "params": [],
            "attrs": [
                # Input
                "iso8601"
            ],
        },
        # An entity corresponding to an InfluxDB measurement.
        # TODO: Maybe add a way to also set tags.
        "Measurement": {
            "public": True,
            "any_inputs": True,
            "params": ["name"],
            "attrs": [],
        },
    },
}


class InfluxSink(mosaik_api.Simulator):
    bucket: str
    org: str
    write_api: influxdb_client.WriteApi
    has_datetime: bool
    measurements: Set[str]

    def __init__(self):
        super().__init__(META)

    def init(self, sid, time_resolution, url, token, org, bucket):
        self.bucket = bucket
        self.org = org
        client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)
        self.write_api = client.write_api(write_options=SYNCHRONOUS)
        self.has_datetime = False
        self.measurements = set()
        return self.meta

    def create(self, num, model, **params):
        if model == "DateTime":
            if num != 1 or self.has_datetime:
                raise ValueError("Cannot create more than one DateTime entity.")
            self.has_datetime = True
            return [{"eid": "DateTime", "type": "DateTime"}]

        if model == "Measurement":
            if num != 1:
                raise ValueError("Cannot create several measurements of the same name.")
            name = params["name"]
            if name in self.measurements:
                raise ValueError(f"Measurement {name} already exists.")
            if name == "DateTime":
                raise ValueError(f"'DateTime' is reserved for the DateTime entity.")

            # TODO: Decouple entity id and measurement name?
            self.measurements.add(name)
            return [{"eid": name, "type": model}]

        raise ValueError(f"Model type {model} does not exist.")

    def step(self, time, inputs, max_advance):
        if "DateTime" in inputs:
            datetime = next(
                iter(inputs["DateTime"]["iso8601"].values())
            )  # extract first value
            del inputs["DateTime"]
        else:
            datetime = None

        for eid, einputs in inputs.items():
            for attr, values in einputs.items():
                for src_full_id, value in values.items():
                    record = (
                        influxdb_client.Point(eid)
                        .tag("src_full_id", src_full_id)
                        .field(attr, value)
                    )
                    if datetime:
                        record.time(datetime)
                    self.write_api.write(
                        bucket=self.bucket,
                        org=self.org,
                        record=record,
                    )

    def get_data(self, outputs):
        return {}


def main():
    mosaik_api.start_simulation(InfluxSink())


if __name__ == "__main__":
    main()
